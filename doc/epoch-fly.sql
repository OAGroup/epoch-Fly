-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.7.22-log - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 导出 epoch-fly 的数据库结构
CREATE DATABASE IF NOT EXISTS `epoch-fly` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `epoch-fly`;

-- 导出  表 epoch-fly.fly_area 结构
CREATE TABLE IF NOT EXISTS `fly_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '专区名称',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='专区';

-- 正在导出表  epoch-fly.fly_area 的数据：~0 rows (大约)
DELETE FROM `fly_area`;
/*!40000 ALTER TABLE `fly_area` DISABLE KEYS */;
/*!40000 ALTER TABLE `fly_area` ENABLE KEYS */;

-- 导出  表 epoch-fly.fly_level_set 结构
CREATE TABLE IF NOT EXISTS `fly_level_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '等级名称',
  `exp` int(11) DEFAULT NULL COMMENT '所需经验值',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  `is_vip` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- 正在导出表  epoch-fly.fly_level_set 的数据：~8 rows (大约)
DELETE FROM `fly_level_set`;
/*!40000 ALTER TABLE `fly_level_set` DISABLE KEYS */;
INSERT INTO `fly_level_set` (`id`, `name`, `exp`, `order_num`, `is_vip`) VALUES
	(1, '普通会员', 200, 1, 0),
	(2, 'VIP1', 500, 2, 1),
	(3, 'VIP2', 1000, 3, 1),
	(4, 'VIP3', 2000, 4, 1),
	(5, 'VIP4', 5000, 5, 1),
	(6, '顶级VIP', 20000, 6, 1),
	(7, '至尊VIP', 50000, 7, 1),
	(8, '社区创始人', 99999999, 8, 1);
/*!40000 ALTER TABLE `fly_level_set` ENABLE KEYS */;

-- 导出  表 epoch-fly.fly_msg 结构
CREATE TABLE IF NOT EXISTS `fly_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `phone` varchar(50) DEFAULT NULL COMMENT '手机号码',
  `code` varchar(50) DEFAULT NULL COMMENT '短信编码',
  `msg` varchar(500) DEFAULT NULL COMMENT '短信内容',
  `expire_date` bigint(20) DEFAULT NULL COMMENT '过期时间',
  `c_time` time DEFAULT NULL COMMENT '创建时间，每次查询最新的这个',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 正在导出表  epoch-fly.fly_msg 的数据：~0 rows (大约)
DELETE FROM `fly_msg`;
/*!40000 ALTER TABLE `fly_msg` DISABLE KEYS */;
/*!40000 ALTER TABLE `fly_msg` ENABLE KEYS */;

-- 导出  表 epoch-fly.fly_my_msg 结构
CREATE TABLE IF NOT EXISTS `fly_my_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '推送人',
  `to_user_id` int(11) DEFAULT NULL COMMENT '被推送人',
  `post_id` int(11) DEFAULT NULL COMMENT '帖子ID',
  `c_time` datetime DEFAULT NULL COMMENT '消息推送时间',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 正在导出表  epoch-fly.fly_my_msg 的数据：~0 rows (大约)
DELETE FROM `fly_my_msg`;
/*!40000 ALTER TABLE `fly_my_msg` DISABLE KEYS */;
/*!40000 ALTER TABLE `fly_my_msg` ENABLE KEYS */;

-- 导出  表 epoch-fly.fly_post 结构
CREATE TABLE IF NOT EXISTS `fly_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `popularity` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `price_type` char(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment_status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `hits` int(11) DEFAULT NULL,
  `comment` int(11) DEFAULT NULL,
  `accept` tinyint(4) DEFAULT NULL,
  `sticky_post` tinyint(4) DEFAULT '0',
  `fine_post` tinyint(4) DEFAULT '0',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='帖子';

-- 正在导出表  epoch-fly.fly_post 的数据：~0 rows (大约)
DELETE FROM `fly_post`;
/*!40000 ALTER TABLE `fly_post` DISABLE KEYS */;
/*!40000 ALTER TABLE `fly_post` ENABLE KEYS */;

-- 导出  表 epoch-fly.fly_post_collection 结构
CREATE TABLE IF NOT EXISTS `fly_post_collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `c_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 正在导出表  epoch-fly.fly_post_collection 的数据：~0 rows (大约)
DELETE FROM `fly_post_collection`;
/*!40000 ALTER TABLE `fly_post_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `fly_post_collection` ENABLE KEYS */;

-- 导出  表 epoch-fly.fly_post_type 结构
CREATE TABLE IF NOT EXISTS `fly_post_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `c_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_new` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='帖子的类型表';

-- 正在导出表  epoch-fly.fly_post_type 的数据：~6 rows (大约)
DELETE FROM `fly_post_type`;
/*!40000 ALTER TABLE `fly_post_type` DISABLE KEYS */;
INSERT INTO `fly_post_type` (`id`, `name`, `code`, `order_num`, `c_time`, `is_new`, `status`) VALUES
	(1, '公告', 'news', 6, NULL, '1', '0'),
	(2, '提问', 'quiz', 1, NULL, '1', '1'),
	(3, '分享', 'share', 2, NULL, '1', '1'),
	(4, '公告', 'notice', 5, NULL, '1', '0'),
	(5, '讨论', 'discuss', 3, NULL, '1', '1'),
	(6, '建议', 'suggest', 4, NULL, '1', '1');
/*!40000 ALTER TABLE `fly_post_type` ENABLE KEYS */;

-- 导出  表 epoch-fly.fly_product 结构
CREATE TABLE IF NOT EXISTS `fly_product` (
  `id` int(11) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `authProduct` varchar(50) NOT NULL,
  `attribute` varchar(50) NOT NULL,
  `expiry_time` datetime DEFAULT NULL,
  `pay_mount` decimal(10,0) NOT NULL,
  `dowload` varchar(50) DEFAULT NULL,
  `c_time` datetime NOT NULL,
  `auth_user_id` int(11) NOT NULL,
  `is_expiry` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 正在导出表  epoch-fly.fly_product 的数据：~0 rows (大约)
DELETE FROM `fly_product`;
/*!40000 ALTER TABLE `fly_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `fly_product` ENABLE KEYS */;

-- 导出  表 epoch-fly.fly_question 结构
CREATE TABLE IF NOT EXISTS `fly_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text,
  `answer` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='问题库';

-- 正在导出表  epoch-fly.fly_question 的数据：~0 rows (大约)
DELETE FROM `fly_question`;
/*!40000 ALTER TABLE `fly_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `fly_question` ENABLE KEYS */;

-- 导出  表 epoch-fly.fly_replay_post 结构
CREATE TABLE IF NOT EXISTS `fly_replay_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `replay_time` datetime DEFAULT NULL,
  `replay_text` longtext COLLATE utf8mb4_unicode_ci,
  `replay_user_id` int(11) DEFAULT NULL,
  `praise_count` int(11) DEFAULT '0',
  `accept` tinyint(2) DEFAULT '0',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='帖子回复';

-- 正在导出表  epoch-fly.fly_replay_post 的数据：~0 rows (大约)
DELETE FROM `fly_replay_post`;
/*!40000 ALTER TABLE `fly_replay_post` DISABLE KEYS */;
/*!40000 ALTER TABLE `fly_replay_post` ENABLE KEYS */;

-- 导出  表 epoch-fly.fly_replay_praise 结构
CREATE TABLE IF NOT EXISTS `fly_replay_praise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `replay_id` int(11) DEFAULT NULL,
  `praise_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='点赞表';

-- 正在导出表  epoch-fly.fly_replay_praise 的数据：~0 rows (大约)
DELETE FROM `fly_replay_praise`;
/*!40000 ALTER TABLE `fly_replay_praise` DISABLE KEYS */;
/*!40000 ALTER TABLE `fly_replay_praise` ENABLE KEYS */;

-- 导出  表 epoch-fly.fly_role 结构
CREATE TABLE IF NOT EXISTS `fly_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色表';

-- 正在导出表  epoch-fly.fly_role 的数据：~0 rows (大约)
DELETE FROM `fly_role`;
/*!40000 ALTER TABLE `fly_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `fly_role` ENABLE KEYS */;

-- 导出  表 epoch-fly.fly_session 结构
CREATE TABLE IF NOT EXISTS `fly_session` (
  `id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `expire_date` bigint(20) NOT NULL COMMENT '过期时间',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户session表';

-- 正在导出表  epoch-fly.fly_session 的数据：~1 rows (大约)
DELETE FROM `fly_session`;
/*!40000 ALTER TABLE `fly_session` DISABLE KEYS */;
INSERT INTO `fly_session` (`id`, `user_id`, `expire_date`) VALUES
	('c880bdc5c486445aafc013928fea3e63', 1, 1537804324987);
/*!40000 ALTER TABLE `fly_session` ENABLE KEYS */;

-- 导出  表 epoch-fly.fly_user 结构
CREATE TABLE IF NOT EXISTS `fly_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `nickname` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `salt` varchar(128) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `email_status` tinyint(2) DEFAULT '0',
  `mobile` varchar(32) DEFAULT NULL,
  `mobile_status` tinyint(2) DEFAULT '0',
  `gender` varchar(16) DEFAULT NULL,
  `signature` varchar(2048) DEFAULT NULL,
  `qq` varchar(50) DEFAULT NULL,
  `head_image` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `company` varchar(256) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `avatar` varchar(256) DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  `last_time` datetime DEFAULT NULL,
  `last_ip` varchar(50) DEFAULT NULL,
  `post_status` tinyint(4) DEFAULT NULL,
  `approve` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 正在导出表  epoch-fly.fly_user 的数据：~0 rows (大约)
DELETE FROM `fly_user`;
/*!40000 ALTER TABLE `fly_user` DISABLE KEYS */;
INSERT INTO `fly_user` (`id`, `nickname`, `password`, `salt`, `email`, `email_status`, `mobile`, `mobile_status`, `gender`, `signature`, `qq`, `head_image`, `birthday`, `company`, `address`, `city`, `avatar`, `status`, `c_time`, `u_time`, `last_time`, `last_ip`, `post_status`, `approve`) VALUES
	(1, 'admin', 'aa383d017a6e269388461072e057438e43592df79e2bb61afb0a7c4f27274b3c', '7629de31cc024cf5a8ab13485b3f9976', 'admin', 1, '15209831990', 0, '1', '26岁<br>Epoch社区开发者', '939961241', NULL, NULL, NULL, NULL, '合肥', '//q.qlogo.cn/qqapp/101235792/F9D188DE2C24AE9590785EA4EC5ADC24/100', 1, '2018-08-01 00:00:00', NULL, NULL, NULL, 1, '大师级程序员');
/*!40000 ALTER TABLE `fly_user` ENABLE KEYS */;

-- 导出  表 epoch-fly.fly_user_sign 结构
CREATE TABLE IF NOT EXISTS `fly_user_sign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `sign_time` datetime DEFAULT NULL,
  `sign_count` int(11) DEFAULT NULL,
  `sign_all_count` int(11) DEFAULT NULL,
  `sign_message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exp` int(11) DEFAULT NULL,
  `kiss` int(11) DEFAULT NULL,
  `gold` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `level_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level_is_vip` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- 正在导出表  epoch-fly.fly_user_sign 的数据：~0 rows (大约)
DELETE FROM `fly_user_sign`;
/*!40000 ALTER TABLE `fly_user_sign` DISABLE KEYS */;
INSERT INTO `fly_user_sign` (`id`, `user_id`, `sign_time`, `sign_count`, `sign_all_count`, `sign_message`, `exp`, `kiss`, `gold`, `level`, `level_name`, `level_is_vip`) VALUES
	(31, 1, '2018-09-24 21:51:23', 1, 1, NULL, 0, NULL, 0, 1, '普通会员', 0);
/*!40000 ALTER TABLE `fly_user_sign` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
