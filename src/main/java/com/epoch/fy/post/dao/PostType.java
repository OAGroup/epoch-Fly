package com.epoch.fy.post.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;
/**
 * 
* <p>Title: PostType.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ModelBind(table = "fly_post_type")
public class PostType extends BaseModel<PostType> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final PostType dao = new PostType();
	
	public static final String ID = "ID"; // 主键id
	
	public static final String NAME = "NAME";
	public static final String ORDER_NUM = "ORDER_NUM";
	public static final String IS_NEW = "IS_NEW";
	public static final String STATUS = "STATUS";
	public static final String CODE = "CODE";
}
