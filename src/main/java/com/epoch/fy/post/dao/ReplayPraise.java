package com.epoch.fy.post.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;
/**
 * 
* <p>Title: ReplayPraise.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ModelBind(table = "fly_replay_praise")
public class ReplayPraise extends BaseModel<ReplayPraise> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final ReplayPraise dao = new ReplayPraise();
	
	public static final String ID = "ID"; // 主键id
	public static final String REPLAY_ID = "REPLAY_ID";//点赞的评论回复的ID
	public static final String PRAISE_USER_ID = "PRAISE_USER_ID";//点赞人
}
