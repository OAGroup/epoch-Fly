package com.epoch.fy.post.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;
/**
 * 
* <p>Title: MyMsg.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ModelBind(table = "fly_my_msg")
public class MyMsg extends BaseModel<MyMsg> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final MyMsg dao = new MyMsg();
	
	public static final String ID = "ID"; // 主键id
	public static final String USER_ID = "USER_ID";
	public static final String TO_USER_ID = "TO_USER_ID";
	public static final String POST_ID = "POST_ID";
}
