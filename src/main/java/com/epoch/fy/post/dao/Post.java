package com.epoch.fy.post.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;
/**
 * 
* <p>Title: Post.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ModelBind(table = "fly_post")
public class Post extends BaseModel<Post> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final Post dao = new Post();
	
	public static final String ID = "ID"; // 主键id
	public static final String TYPE_ID = "TYPE_ID";
	public static final String USER_ID = "USER_ID";
	public static final String TITLE = "TITLE";
	public static final String CONTENT = "CONTENT";
	public static final String PRICE = "PRICE";
	public static final String HITS = "HITS";
	public static final String ACCEPT = "ACCEPT";
	public static final String STICKY_POST = "STICKY_POST";
	public static final String FINE_POST = "FINE_POST";
	public static final String STATUS = "STATUS";
}
