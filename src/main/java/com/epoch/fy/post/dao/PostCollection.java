package com.epoch.fy.post.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;
/**
 * 
* <p>Title: PostCollection.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ModelBind(table = "fly_post_collection")
public class PostCollection extends BaseModel<PostCollection> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final PostCollection dao = new PostCollection();
	
	public static final String ID = "ID"; // 主键id
	public static final String POST_ID = "POST_ID";
	public static final String USER_ID = "USER_ID";
}
