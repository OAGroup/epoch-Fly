package com.epoch.fy.post.controller;

import java.io.File;

import com.epoch.base.annotation.ControllerBind;
import com.epoch.base.controller.BaseController;
import com.epoch.base.interceptor.FrontAuthInterceptor;
import com.epoch.base.interceptor.UserAuthInterceptor;
import com.epoch.base.oss.OSSFactory;
import com.epoch.fy.post.dao.MyMsg;
import com.epoch.fy.post.dao.Post;
import com.epoch.fy.post.dao.PostCollection;
import com.epoch.fy.post.dao.ReplayPost;
import com.epoch.fy.user.dao.Product;
import com.epoch.fy.user.dao.User;
import com.epoch.fy.user.dao.UserSign;
import com.epoch.fy.user.service.UserService;
import com.jfinal.aop.Before;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.jfinal.upload.UploadFile;
/**
 * 
* <p>Title: ApiController.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ControllerBind(controllerKey = "/api")
public class ApiController extends BaseController{
	
	@Before(FrontAuthInterceptor.class)
	public void upload() {
		String contentType = getRequest().getContentType();
        boolean flag = contentType != null && contentType.toLowerCase().indexOf("multipart") != -1;
        if(!flag) {
        	renderJson(Ret.fail().set("message", "不是MultipartRequest，不能上传文件"));
            return;
        }
        
        UploadFile uploadFile = getFile();
        if (uploadFile == null) {
            renderJson(Ret.fail().set("message", "没有接收到任何文件上传"));
            return;
        }
        File file = uploadFile.getFile();
        long size_check = 2 * 1024 * 1024;
        long usr_upLoad_file_size = file.length();
        if (usr_upLoad_file_size < size_check) {
        	String path = OSSFactory.build().upload(file);
        	file.delete();
            renderJson(Ret.ok().set("path",path).set("message", "上传成功"));
        } else {
            file.delete();
            renderFail("自定义图片请限制在2M以内!");
        }
	}
	
	@Before(UserAuthInterceptor.class)
	public void collectionAdd() {
		String id = getPara("id");
		Integer user_id = getUserId();
		Kv param = Kv.create();
		param.set("post_id", id);
		param.set("user_id", user_id);
		SqlPara sqlPara = Db.getSqlPara("fly.findPostCollection", param);
		PostCollection postCollection  = PostCollection.dao.findFirst(sqlPara);
		if (null == postCollection) {
			postCollection = new PostCollection();
			postCollection.set(PostCollection.POST_ID, id);
			postCollection.set(PostCollection.USER_ID, user_id);
			postCollection.save();
			
		}
		renderSuccess("收藏成功");
	}
	
	@Before(FrontAuthInterceptor.class)
	public void collectionDel() {
		String id = getPara("id");
		Integer user_id = getUserId();
		Kv param = Kv.create();
		param.set("post_id", id);
		param.set("user_id", user_id);
		SqlPara sqlPara = Db.getSqlPara("fly.findPostCollection", param);
		PostCollection postCollection  = PostCollection.dao.findFirst(sqlPara);
		if (null != postCollection) {
			postCollection.delete();
		}
		renderSuccess("取消收藏成功");
	}
	
	@Before(FrontAuthInterceptor.class)
	public void commentAdopt() {
		String post_id = getPara("post_id");
		String replay_id = getPara("id");
		Kv param = Kv.create();
		param.set("post_id", post_id);
		SqlPara sqlPara = Db.getSqlPara("fly.findPostPlayIsAccept", param);
		ReplayPost post = ReplayPost.dao.findFirst(sqlPara);
		if(null == post) {
			ReplayPost replay = ReplayPost.dao.findById(replay_id);
			Integer user_id = replay.getInt(ReplayPost.REPLAY_USER_ID);
			if(user_id == getUserId()) {
				renderSuccess("采纳失败,不能与当前登录人相同!");
			}else {
				Post post_ = Post.dao.findById(replay.getInt(ReplayPost.POST_ID));
				replay.set(ReplayPost.ACCEPT, 1);
				replay.update();
				UserSign userSign = UserService.me.getUserSign(user_id);
				userSign.set(User.GOLD, userSign.getInt(User.GOLD)+post_.getInt(Post.PRICE));
				userSign.update();
				renderSuccess("采纳成功");
			}
		}else {
			renderSuccess("采纳失败");
		}
	}
	
	@Before(FrontAuthInterceptor.class)
	public void message() {
		
	}
	
	@Before(FrontAuthInterceptor.class)
	public void messageNum() {
		Integer user_id = getUserId();
		Kv param = Kv.create();
		param.set("user_id", user_id);
		SqlPara sqlPara = Db.getSqlPara("fly.findMyMsgCount", param);
		MyMsg msg = MyMsg.dao.findFirst(sqlPara);
		Integer count = msg.getInt("count");
		count = count == null ? 0:count;
		renderSuccess("",Ret.by("status", 0).set("count", count));
	}
	
	@Before(FrontAuthInterceptor.class)
	public void productList() {
		Kv param = commonPostParams();
		Integer userId = getUserId();
		param.set("user_id", userId);
		SqlPara sqlPara = Db.getSqlPara("fly.findMyProduct", param);
		Page<Product> productList= Product.dao.paginate(sqlPara, param);
		renderJson(productList);
		
	}
	
	@Before(FrontAuthInterceptor.class)
	public void download() {
		Integer userId = getUserId();
		String proId = getPara("proId");
		Kv param = Kv.create();
		param.set("user_id", userId);
		param.set("proId", proId);
		SqlPara sqlPara = Db.getSqlPara("fly.findMyProduct", param);
		Product product = Product.dao.findFirst(sqlPara);
		if(null == product) {
			renderFail("没有权限");
		}else {
			String targetPath = "";
			renderFile(new File(targetPath));
		}
	}
	
}
