package com.epoch.fy.post.controller;

import java.util.Date;
import java.util.List;

import com.epoch.base.annotation.ControllerBind;
import com.epoch.base.controller.BaseController;
import com.epoch.base.interceptor.UserAuthInterceptor;
import com.epoch.fy.post.dao.Post;
import com.epoch.fy.post.dao.ReplayPost;
import com.epoch.fy.post.dao.ReplayPraise;
import com.epoch.fy.post.service.PostService;
import com.epoch.fy.user.dao.User;
import com.jfinal.aop.Before;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
/**
 * 
* <p>Title: JieController.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ControllerBind(controllerKey = "/jie")
public class JieController extends BaseController {

    public void page() {
        String view = getAttr("view");
        Kv param = Kv.create();
        param.put("id", view);
        SqlPara sqlPara = Db.getSqlPara("fly.findPostDetail", param);
        Post detail = Post.dao.findFirst(sqlPara);
        SqlPara sqlPara_ = Db.getSqlPara("fly.findReplayPostList", param);
        List<ReplayPost> replayPostList = ReplayPost.dao.find(sqlPara_);
        sqlPara = Db.getSqlPara("fly.findPostPlayWeekHeat", param);
        List<Post> weekHeatReplayList = Post.dao.find(sqlPara);
        setAttr("weekHeatReplayList", weekHeatReplayList);
        setAttr("postDetail", detail);
        setAttr("replayList", replayPostList);
        setAttr("postId", view);

        detail.set(Post.HITS, detail.getInt(Post.HITS) + 1);
        detail.update();

        render("/community/post/detail.html");
    }

    @Before(UserAuthInterceptor.class)
    public void edit() {
        String view = getAttr("view");
        System.out.println(view);
        Kv param = Kv.create();
        param.put("id", view);
        SqlPara sqlPara = Db.getSqlPara("fly.findPostDetail2", param);
        Post detail = Post.dao.findFirst(sqlPara);
        setAttr("postDetail", detail);
        render("/community/post/new.html");
    }

    @Before(UserAuthInterceptor.class)
    public void reply() {
        String content = getPara("content");
        String jie = getPara("jid");
        ReplayPost replay = new ReplayPost();
        replay.set(ReplayPost.POST_ID, jie);
        replay.set(ReplayPost.PRAISE_COUNT, 0);
        replay.set(ReplayPost.ACCEPT, 0);
        replay.set(ReplayPost.REPLAY_USER_ID, getUserId());
        replay.set(ReplayPost.REPLAY_TEXT, content);
        replay.set(ReplayPost.REPLAY_TIME, new Date());
        replay.save();
        Ret ret = Ret.create();
        ret.set("content", content);
        ret.set("type", "reply");
        ret.set("user", getUser(getUserId()));
        PostService.me.setMsg(content, getUserId(), Integer.parseInt(jie));
        renderSuccess("回复成功", ret);
    }

    public void jump() {
        String username = getPara("username");
        Kv param = Kv.create();
        param.set("nickname", username);
        SqlPara sqlPara = Db.getSqlPara("fly.findUserByNickName", param);
        User user = User.dao.findFirst(sqlPara);
        redirect("/my/u/" + user.getInt(User.ID));
    }

    @Before(UserAuthInterceptor.class)
    public void zan() {
        String id = getPara("id");
        //先拿当前用户和评论ID查询是否已经点赞，如果点赞则不能重复点赞
        Kv param = Kv.create();
        param.set("replay_id", id);
        param.set("user_id", getUserId());
        SqlPara sqlPara = Db.getSqlPara("fly.findReplayPraise", param);
        ReplayPraise replay = ReplayPraise.dao.findFirst(sqlPara);
        if (null == replay) {
            replay = new ReplayPraise();
            replay.set(ReplayPraise.REPLAY_ID, Integer.parseInt(id));
            replay.set(ReplayPraise.PRAISE_USER_ID, getUserId());
            replay.save();
            ReplayPost post = ReplayPost.dao.findById(id);
            post.set(ReplayPost.PRAISE_COUNT, post.getInt(ReplayPost.PRAISE_COUNT) + 1);
            post.update();
            renderSuccess("点赞成功!");
        } else {
            renderFail("已经点赞，请勿重复点赞!");
        }

    }
}
