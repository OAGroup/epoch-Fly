package com.epoch.fy.post.controller;

import com.epoch.base.annotation.ControllerBind;
import com.epoch.base.controller.BaseController;
import com.jfinal.kit.Kv;
/**
 * 
* <p>Title: ColumnPostController.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ControllerBind(controllerKey = "/column")
public class ColumnPostController  extends BaseController{
	
	public void quiz() {
		Kv param = commonPostParams();
		param.put("type", "quiz");
		renderPost(param);
	}
	
	public void all() {
		Kv param = commonPostParams();
		param.put("type", "all");
		renderPost(param);
	}
	
	public void discuss() {
		Kv param = commonPostParams();
		param.put("type", "discuss");
		renderPost(param);
	}
	
	public void share() {
		Kv param = commonPostParams();
		param.put("type", "share");
		renderPost(param);
	}
	
	public void news() {
		Kv param = commonPostParams();
		param.put("type", "news");
		renderPost(param);
	}
	
	public void suggest() {
		Kv param = commonPostParams();
		param.put("type", "suggest");
		renderPost(param);
	}
	
	public void notice() {
		Kv param = commonPostParams();
		param.put("type", "notice");
		renderPost(param);
	}
	
}
