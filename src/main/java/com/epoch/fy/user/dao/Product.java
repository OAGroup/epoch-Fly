package com.epoch.fy.user.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;
/**
 * 
* <p>Title: Product.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ModelBind(table = "fly_product")
public class Product extends BaseModel<Product> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final Product dao = new Product();
	
	public static final String ID = "ID"; // 主键id
	public static final String PRODUCT_NAME = "PRODUCT_NAME";
	public static final String ALIAS = "ALIAS";
	public static final String ATTRIBUTE = "ATTRIBUTE";
	public static final String AUTHORIZATION_TIME = "AUTHORIZATION_TIME";
	public static final String PAY_MOUNT = "PAY_MOUNT";
	public static final String DOWLOAD = "DOWLOAD";
	public static final String AUTH_USER_ID = "AUTH_USER_ID";
}
