package com.epoch.fy.user.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;
/**
 * 
* <p>Title: UserSign.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ModelBind(table = "fly_user_sign")
public class UserSign extends BaseModel<UserSign> {
	
	private static final long serialVersionUID = 1L;
	
	public static final UserSign dao = new UserSign();
	
	public static final String ID = "ID"; // 主键id
	public static final String USER_ID = "USER_ID";
	public static final String SIGN_TIME = "SIGN_TIME";
	public static final String SIGN_COUNT = "SIGN_COUNT";
	public static final String SIGN_ALL_COUNT = "SIGN_ALL_COUNT";
	
	public static final String KISS = "KISS";
	public static final String GOLD = "GOLD";
	public static final String LEVEL = "LEVEL";
	public static final String EXP = "EXP";
	
	public static final String LEVEL_NAME = "LEVEL_NAME";
	public static final String LEVEL_IS_VIP = "LEVEL_IS_VIP";
}
