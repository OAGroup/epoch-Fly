package com.epoch.fy.user.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;
/**
 * 
* <p>Title: LevelSet.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ModelBind(table = "fly_level_set")
public class LevelSet extends BaseModel<LevelSet> {
	
	private static final long serialVersionUID = 1L;
	
	public static final LevelSet dao = new LevelSet();
	
	public static final String ID = "ID";
	public static final String NAME = "NAME";
	public static final String EXP = "EXP";
	public static final String IS_VIP = "IS_VIP";
}
