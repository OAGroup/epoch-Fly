package com.epoch.fy.user.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;
/**
 * 
* <p>Title: Session.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ModelBind(table = "fly_session")
public class Session extends BaseModel<Session> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final Session dao = new Session();
	
	public static final String ID = "ID"; // 主键id
	public static final String USER_ID = "USER_ID";//用户ID
	public static final String EXPIRE_DATE = "EXPIRE_DATE";//session过期时间
	
	/**
	 * 登录会话是否已过期
	 */
	public boolean isExpired() {
		return this.getLong(Session.EXPIRE_DATE) < System.currentTimeMillis();
	}

	/**
	 * 登录会话是否未过期
	 */
	public boolean notExpired() {
		return ! isExpired();
	}
}
