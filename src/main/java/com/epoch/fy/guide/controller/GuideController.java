package com.epoch.fy.guide.controller;

import com.epoch.base.annotation.ControllerBind;
import com.epoch.base.controller.BaseController;
/**
 * 
* <p>Title: GuideController.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ControllerBind(controllerKey = "/guide")
public class GuideController extends BaseController{
	
	public void index() {
		render("/guide/guide.html");
	}
}
