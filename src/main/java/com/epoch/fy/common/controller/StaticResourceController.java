package com.epoch.fy.common.controller;
/**
 * 
 */
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.OutputStream;

import com.epoch.base.annotation.ControllerBind;
import com.epoch.base.controller.BaseController;
import com.jfinal.kit.PropKit;

@ControllerBind(controllerKey = "/static")
public class StaticResourceController extends BaseController{
	
	public void file() {
		String view = getAttr("view").toString();
		getResponse().setContentType("application/octet-stream;charset=UTF-8");
		try {
			FileInputStream inputStream = new FileInputStream(PropKit.get("config.upload.basePath", "upload")+view);
			int i=inputStream.available();
			byte[]data=new byte[i];
			inputStream.read(data);
			inputStream.close();

			OutputStream outputStream=new BufferedOutputStream(getResponse().getOutputStream());
			outputStream.write(data);
			outputStream.flush();
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
