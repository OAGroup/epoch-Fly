package com.epoch.base.tag;

import org.beetl.core.Resource;
import org.beetl.core.ResourceLoader;
import org.beetl.ext.tag.IncludeTag;

import javax.servlet.http.HttpServletRequest;

public class TerminalIncludeTag extends IncludeTag {

	protected String getRelResourceId()
	{

		Resource sibling = ctx.getResource();
		//不要使用resource的loder，因为有可能是
		ResourceLoader resourceLoader = gt.getResourceLoader();
		String key =  gt.getResourceLoader().getResourceId(sibling, (String) this.args[0]);
		HttpServletRequest request = (HttpServletRequest)ctx.getGlobal("request");
		//Object obj = request.getAttribute("isMobile");
		return key;
//		if(obj == null){
//			return key;
//		}
//		boolean isMobile =(Boolean) request.getAttribute("isMobile");
//		if(isMobile){
//			String mobileKey = MobileUtil.genMobileKey(key);
//			if(resourceLoader.exist(mobileKey)){
//				return mobileKey;
//			}else{
//				return key;
//			}
//		}else{
//			return key;
//		}
				
		
	}
}
