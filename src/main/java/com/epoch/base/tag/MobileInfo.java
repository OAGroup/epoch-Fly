package com.epoch.base.tag;

public class MobileInfo {
	String[] attrs = null;
	public MobileInfo(String mobile){
		if(mobile==null||mobile.length()==0){
			attrs = new String[0];
			
		}else{
			attrs = mobile.split(" ");
		}		
	}
	
	public boolean isHidden(){
		return find("hidden")!=-1;
	}
	
	public  boolean isShow(){
		return find("show")!=-1;
	}
	public  boolean isDropdown(){
		return find("dropdown")!=-1;
	}
	
	public  boolean isFadein(){
		return find("fadein")!=-1;
	}
	
	
	
	private int find(String key){
		for(int i=0;i<attrs.length;i++){
			 if(attrs[i].equals(key)){
				 return i;
			 }
		}
		return -1;
	}
}
