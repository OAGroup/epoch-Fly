package com.epoch.base.tag;

import org.apache.commons.collections.MapUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TagChildrenContext {
		private List<HTMLTag> children = null;
	//保存子级有多种标签
	private Map<String, List<HTMLTag>> childrenMap = new HashMap<String, List<HTMLTag>>();
	public List<HTMLTag> getChildren() {
		if(children==null) {
			children = new ArrayList<HTMLTag>();
		}
		return children;
	}
	public void setChildren(List<HTMLTag> children) {
		this.children = children;
	}

	public Map<String, List<HTMLTag>> getChildrenMap() {
		for (HTMLTag child : children) {
			String tagName = child.getTagName();
			if (childrenMap.containsKey(tagName)) {
				List<HTMLTag> tempList = (List<HTMLTag>) MapUtils.getObject(childrenMap, tagName);
				tempList.add(child);
			} else {
				List<HTMLTag> tempList = new ArrayList<HTMLTag>();
				tempList.add(child);
				childrenMap.put(tagName, tempList);
			}
		}
		return childrenMap;
	}
}
