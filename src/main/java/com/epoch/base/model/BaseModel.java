package com.epoch.base.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.epoch.base.util.DateUtils;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.jfinal.plugin.activerecord.Table;
import com.jfinal.plugin.activerecord.TableMapping;
/**
 * 
* <p>Title: BaseModel.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@SuppressWarnings("rawtypes")
public class BaseModel<M extends Model> extends Model<M>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    public static final String CREATE_BY = "CREATE_BY";
    public static final String CREATE_DATE = "CREATE_DATE";
    public static final String UPDATE_DATE = "UPDATE_DATE";
    public static final String UPDATE_BY = "UPDATE_BY";
	
	@Override
	public Integer getInt(String attr) {
		Object obj = get(attr);
		if (obj == null) {
			return null;
		} else if (obj instanceof Integer) {
			return (Integer) obj;
		} else if (obj instanceof BigDecimal) {
			return ((BigDecimal) obj).intValue();
		} else if (obj instanceof String) {
			try {
				return Integer.parseInt((String) obj);
			} catch (Exception e) {
				throw new RuntimeException("String can not cast to Integer : " + attr.toString());
			}
		} else {
			try {
				return Integer.parseInt(obj.toString());
			} catch (Exception e) {
				throw new RuntimeException("Object can not cast to Integer : " + attr.toString());
			}
		}
	}
	
	public Page<M> paginate(Kv params, String select, String sqlPara, Object... paras) {
		return paginate(params.getInt("pageNumber"), params.getInt("pageSize"), select, sqlPara, paras);
	}
	
	public Table getTable() {
		return TableMapping.me().getTable(getClass());
	}
	
	public M selectOne(SqlPara sqlPara) {
		List<M> list = this.find(sqlPara);
		return list.get(0);
	}
	
    private Class<? extends Model> getUsefulClass() {
        Class c = getClass();
        return c.getName().indexOf("EnhancerByCGLIB") == -1 ? c : c
                .getSuperclass(); // com.demo.blog.Blog$$EnhancerByCGLIB$$69a17158
    }
    
    
    
	@Override
	public boolean save() {
		Table table = TableMapping.me().getTable(getUsefulClass());
        Map<String, Class<?>> columnAll = table.getColumnTypeMap();
        Date now = DateUtils.getCurrentDate();
        if (columnAll.containsKey("c_time") && this.get("c_time") == null) {
            this.set("c_time", now);
        }
        if (columnAll.containsKey("u_time") && this.get("u_time") == null) {
            this.set("u_time", now);
        }
		return super.save();
	}
	
	public Page<M> paginate(SqlPara sqlPara,Kv params) {
        return super.paginate(params.getInt("page"), params.getInt("limit"), sqlPara);
    }
}
