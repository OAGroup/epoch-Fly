package com.epoch.base.util;

import com.jfinal.kit.PathKit;
import com.jfinal.upload.UploadFile;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
/**
 * 
* <p>Title: AttachmentUtils.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
public class AttachmentUtils {

    static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

    /**
     * @param uploadFile
     * @return new file relative path
     */
    public static String moveFile(UploadFile uploadFile) {
        if (uploadFile == null)
            return null;

        File file = uploadFile.getFile();
        if (!file.exists()) {
            return null;
        }

        String webRoot = PathKit.getWebRootPath();

        String uuid = UUID.randomUUID().toString().replace("-", "");

        StringBuilder newFileName = new StringBuilder(webRoot).append(File.separator).append("attachment")
                .append(File.separator).append(dateFormat.format(new Date())).append(File.separator).append(uuid)
                .append(FileUtils.getSuffix(file.getName()));

        File newfile = new File(newFileName.toString());

        if (!newfile.getParentFile().exists()) {
            newfile.getParentFile().mkdirs();
        }

        file.renameTo(newfile);

        return FileUtils.removePrefix(newfile.getAbsolutePath(), webRoot);
    }

    public static File createFile(String suffix) {
        String webRoot = PathKit.getWebRootPath();
        String uuid = UUID.randomUUID().toString().replace("-", "");

        StringBuilder newFileName = new StringBuilder(webRoot).append(File.separator).append("attachment")
                .append(File.separator).append(dateFormat.format(new Date())).append(File.separator).append(uuid)
                .append(suffix);
        return new File(newFileName.toString());
    }

    static List<String> imageSuffix = new ArrayList<String>();

    static {
        imageSuffix.add(".jpg");
        imageSuffix.add(".jpeg");
        imageSuffix.add(".png");
        imageSuffix.add(".bmp");
        imageSuffix.add(".gif");
    }

    public static boolean isImage(String path) {
        String sufffix = FileUtils.getSuffix(path);
        if (StringUtils.isNotBlank(sufffix))
            return imageSuffix.contains(sufffix.toLowerCase());
        return false;
    }
}
