package com.epoch.base.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;
/**
 * 
* <p>Title: BaseHandler.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
public class BaseHandler extends Handler {

	@Override
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		if(target.endsWith("/")) {
			target = target.substring(0, target.length()-1);
		}
		if (target.startsWith("/static/file")) {
			String view = target.substring(target.lastIndexOf("/"));
			request.setAttribute("view", view);
			target = "/static/file";
		}
		if (target.startsWith("/jie")) {
			if(null == getJieRequest(target)) {
				String view = target.substring(target.lastIndexOf("/"));
				if(null != view) {
					view = view.replaceAll("/", "");
				}
				target = target.substring(0,target.lastIndexOf("/"));
				request.setAttribute("view", view);
			}
		}
		if (target.startsWith("/my/u")) {
			if(target.contains("uploadHead") || target.contains("update")) {
			}else {
				String view = target.substring(target.lastIndexOf("/"));
				if(null != view) {
					view = view.replaceAll("/", "");
				}
				request.setAttribute("view", view);
				target = "/my/u";
			}
		}
		next.handle(target, request, response, isHandled);
	}
	
	private String getJieRequest(String pageUrl) {
		String target = null;
		String[] urls= {"/jie/reply","/jie/jump","/jie/zan"};
		for (String string : urls) {
			if(pageUrl.startsWith(string)) {
				target = string;
				break;
			}
		}
		return target;
	}

}
