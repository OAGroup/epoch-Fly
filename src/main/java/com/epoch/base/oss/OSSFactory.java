package com.epoch.base.oss;

public class OSSFactory {

	public static AliyunCloudStorageService build() {
		// 获取云存储配置信息
		CloudStorageConfig config = getCloudStorageConfig();
		return new AliyunCloudStorageService(config);
	}
	
	/**
	 * 这一块我我先写死了，之后改造
	 * @return
	 */
	public static CloudStorageConfig getCloudStorageConfig() {
		CloudStorageConfig config = new CloudStorageConfig();
		config.setAliyunAccessKeyId("##");
		config.setAliyunAccessKeySecret("##");
		config.setAliyunBucketName("##");
		config.setAliyunPrefix("upload");
		config.setAliyunDomain("https://platform-wxmall.oss-cn-beijing.aliyuncs.com/");
		config.setAliyunEndPoint("http://oss-cn-beijing.aliyuncs.com/");
		return config;
	}
}
