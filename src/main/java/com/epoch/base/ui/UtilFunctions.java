package com.epoch.base.ui;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.epoch.fy.post.dao.PostCollection;
import com.epoch.fy.post.dao.ReplayPost;
import com.epoch.fy.post.dao.ReplayPraise;
import com.epoch.fy.user.dao.User;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
/**
 * 
* <p>Title: UtilFunctions.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
public class UtilFunctions {

	public boolean isEqual(Object str1, Object str2) {
		if (str1 == null || str2 == null) {
			return false;
		}
		if (StringUtils.equals(str1.toString(), str2.toString())) {
			return true;
		}
		return false;
	}

	public boolean isEmptyTypeList(List<Object> list) {
		if (list == null) {
			return false;
		}
		if (list.size() != 0) {
			return true;
		}
		return false;
	}

	public boolean isManage(Object userId) {
		if (null == userId || String.valueOf(userId).isEmpty()) {
			return false;
		}
		Integer id = Integer.valueOf(String.valueOf(userId));
		User user = User.dao.findById(id);
		if (null == user) {
			return false;
		} else {
			if (user.getStr(User.EMAIL).equalsIgnoreCase("admin")) {
				return true;
			}
		}
		return false;

	}

	public boolean isNotNullOrEmpty(Object obj) {
		return !isNullOrEmpty(obj);
	}

	public static boolean isNullOrEmpty(Object obj) {
		if (obj == null)
			return true;

		if (obj instanceof CharSequence)
			return ((CharSequence) obj).length() == 0;

		if (obj instanceof Collection)
			return ((Collection) obj).isEmpty();

		if (obj instanceof Map)
			return ((Map) obj).isEmpty();

		if (obj instanceof Object[]) {
			Object[] object = (Object[]) obj;
			if (object.length == 0) {
				return true;
			}
			boolean empty = true;
			for (int i = 0; i < object.length; i++) {
				if (!isNullOrEmpty(object[i])) {
					empty = false;
					break;
				}
			}
			return empty;
		}
		return false;
	}

	// 检测是否已经点赞，如果点赞，则呈现图标不一样，不可逆，也就是说不能取消点赞
	public boolean isZan(Integer user_id, Integer replay_id) {
		Kv param = Kv.create();
		param.set("replay_id", replay_id);
		param.set("user_id", user_id);
		SqlPara sqlPara = Db.getSqlPara("fly.findReplayPraise", param);
		ReplayPraise replay = ReplayPraise.dao.findFirst(sqlPara);
		if (null != replay) {
			return true;
		}
		return false;
	}

	// 检测是否已经收藏，如果收藏，则呈现图标不一样变为取消收藏
	public boolean isCollection(Integer user_id, Integer post_id) {
		Kv param = Kv.create();
		param.set("post_id", post_id);
		param.set("user_id", user_id);
		SqlPara sqlPara = Db.getSqlPara("fly.findPostCollection", param);
		PostCollection postCollection = PostCollection.dao.findFirst(sqlPara);
		if (null != postCollection) {
			return true;
		}
		return false;
	}

	/**
	 * 检测帖子是否已经 采纳某一评论，如果采纳，则返回true
	 * @param user_id
	 * @param post_id
	 * @return
	 */
	public boolean isAccept(Integer post_id) {
		Kv param = Kv.create();
		param.set("post_id", post_id);
		SqlPara sqlPara = Db.getSqlPara("fly.findPostPlayIsAccept", param);
		ReplayPost post = ReplayPost.dao.findFirst(sqlPara);
		if (null != post) {
			return true;
		}
		return false;
	}
}
