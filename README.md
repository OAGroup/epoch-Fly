# epochFly

#### Epoch是什么？

Epoch是我个人的一个开源组织，旗下在之后设计到权限框架，工作流，等核心权限构造系统，以及之后的所有的核心系统，都将以Epoch命名，在年底将发
布开源一个Epoch的核心权限系统。

Epoch开源总群：607328652

#### 项目介绍
Jfinal+Beetl+ehcache+Mysql开发的一个社区文档。

参考：

https://fly.layui.com/

https://gitee.com/sentsin/fly

在这里，非常感谢贤心大神的UI，感谢Jfinal技术。

#### 软件架构

后端架构：Jfinal+ehcache+Beetl

前端：Fly社区代码

#### 安装教程

1.安装JDK，使用Tomcat启动即可
2.默认用户管理员为admin,密码123456

#### 可以参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


功能列表

![输入图片说明](https://images.gitee.com/uploads/images/2018/0924/214020_cfaf6871_626204.png "屏幕截图.png")


案例：http://fly2you.cn/community


界面参考截图

![输入图片说明](https://images.gitee.com/uploads/images/2018/0924/215920_3446385a_626204.png "屏幕截图.png")


如果你想快速入门并且进行二次开发，请与我获取收费文档。

赞助50元即可，支付宝转账时候，请一定记得附上邮箱，我会在0-2天内发送到您的邮箱，后续有更新也会一直发送。

![输入图片说明](https://images.gitee.com/uploads/images/2018/0924/215818_0251fbfe_626204.png "屏幕截图.png")


如果你喜欢这个项目，给个star
